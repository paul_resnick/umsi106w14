import test
import copy

alphabet = " !#$%&()*,-./0123456789:;?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]abcdefghijklmnopqrstuvwxyz\'\""
reverse_sorted_letters = ['\xbb', '\xbf', '\xef', '\xa0', '#', '\xa8', '[', '\xa2', '%', ']', '$', '@', 'Z', '&', 'X', '\xa9', '\xc3', 'Q', '9', '7', '(', ')', '/', '3', '*', '5', '4', '6', '2', '8', ':', 'K', 'U', 'V', '0', 'J', '1', 'z', ';', 'F', 'D', 'G', 'R', 'P', 'L', '!', 'N', 'E', 'C', 'O', 'q', 'j', 'Y', 'B', 'x', '?', 'M', 'W', 'A', 'S', '-', 'T', 'H', "'", 'k', 'I', 'v', '"', 'b', '.', 'p', ',', 'g', 'f', 'y', 'c', 'w', 'm', '\n', 'u', 'l', 'd', 'r', 's', 'i', 'h', 'n', 'o', 'a', 't', 'e', ' ']
caps = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"   

def count_guesses(next_letter, guesses):
    try:
        # print "%s took %d guesses from %s" % (next_letter, guesses.index(next_letter) + 1, "".join(guesses))
        return guesses.index(next_letter) + 1
    except:
        print "%s is not among the guesses made; guesses should contain all the alphabet letters, in some order.\n\t%s" % (next_letter, guesses)

class Tracker():
    """Keeps track of the frequency of a letter and a dictionary of Trackers 
    for all next letters for that letter"""
    def __init__(self, count = 0):
        self.count = count
        self.next = {}

class Guesser():
    def __init__(self):
        self.alphabet = alphabet
        # start with an artificial set of letter frequencies that can be overriden by the training data
        self.letter_freqs = {}
        for (i, c) in enumerate(reverse_sorted_letters):
            self.letter_freqs[c] = Tracker(i)
    
    def update_counts(self, prev_txt):
        freqs = self.letter_freqs
        for c in prev_txt[-4:]:
            if c not in freqs:
                freqs[c] = Tracker()
            freqs[c].count += 1
            freqs = freqs[c].next

    def lookup(self, ending):
        """Look up the continuations for the complete, possibly multi-character ending. Return all possible next letters sorted by their frequency"""
        freqs = self.letter_freqs
        try:
            for c in ending:
                freqs = freqs[c].next
        except:
            return []
        return sorted(freqs.keys(), key = lambda x: freqs[x].count, reverse=True)

            
    def guess(self, prev_txt):
        guesses = []
        try:
            guesses += self.lookup(prev_txt[-3:])
        except:
            pass
        try:
            guesses += [c for c in self.lookup(prev_txt[-2:]) if c not in guesses]
        except:
            pass
        try:
            guesses += [c for c in self.lookup(prev_txt[-1]) if c not in guesses]
        except:
            pass
        guesses += [c for c in self.lookup("") if c not in guesses]
        guesses += [k for k in self.alphabet if k not in guesses]
        return guesses
            
    
    def play_game(self, txt, skip_first_let = False, learning=False):
        if not learning:
            # hold onto current stay of self.letter_freqs so you can restore it at the end
            saved_freqs = copy.deepcopy(self.letter_freqs)
        # accumulate the text that's been revealed
        if skip_first_let:
            revealed_text = txt[0]
            txt = txt[1:]
        else:
            revealed_text = ""
        # accumulate the total guess count
        total_guesses = 0
        # accumulate the total characters to be guessed
        total_chars = 0
        # Loop through the letters in the text, making a guess for each
        for c in txt:
            if c in alphabet: # skip letters not in our alphabet; don't have to guess them
                guesses = self.guess(revealed_text)
                total_chars += 1
                total_guesses += count_guesses(c, guesses)
                revealed_text += c
                self.update_counts(revealed_text)
                
        if not learning:
            # forget everything you learned from this txt
            self.letter_freqs = saved_freqs
        return (total_chars, total_guesses)
    
def output_result(min_guesses, actual_guesses):
    print "Score: %.2f\t Minimum guesses: %d\tActual guesses: %d" % (float(actual_guesses) / min_guesses, min_guesses, actual_guesses)


### create an instance of Guesser ####
g = Guesser()

test_txt = "This is some testing text. Yes it is."

### Guess the letters in some text ####
min_g, actual_g = g.play_game(test_txt)
output_result(min_g, actual_g)

### Run it not counting guesses on the first letter; it's always hard to guess the first letter, and it makes it look like short messages have higher entropy than longer messages
print "--- not counting the first letter, which is always hard to guess ---"
min_g, actual_g = g.play_game(test_txt, skip_first_let = True)
output_result(min_g, actual_g)

### Run it in "learning" mode, where it will remember the letter combinations from this text the next time you run it.
print "--- in learning mode, so that it remembers letter combinations from this text ---"
min_g, actual_g = g.play_game(test_txt, learning = True)
output_result(min_g, actual_g)

print "--- remembering previous run, so guesses better now ---"
min_g, actual_g = g.play_game(test_txt)
output_result(min_g, actual_g)