#########Summary of what you'll do on this assignment#####

# 1) Fill in the definition of the class Post to hold information about one post that you've made on Facebook.
    # Add to the __init__ method additional code to set the instance variables comments and likes. 
    # You need to pull out the appropriate data from the json representation of a single post. 
    # You can find a sample in the file samplepost.txt. There are tests that check whether you've pulled out the right data.

# 2) In the Post class, add three methods:
    # -- positive() returns the number of words in the message that are in the list of positive words pos_ws (provided in our code)
    # -- negative() returns the number of words in the message taht are in the list of negative words neg_ws (provided in our code)
    # -- emo_score returns the difference between positive and negative scores
    
    # make the three tests pass as a way of checking that you have defined the methods correctly

# 3) Get a json-formatted version of your last 100 posts on Facebook.

# 4) For each of those posts
    # -- create an instance of your class Post, filling in attributes for:
        #-- the message if it exists
        #-- the comments data if it exists
        #-- the likes data if it exists

# 5) Compute the top three likers and commenters on your posts (the people who did the most comments and likes)

# 6) Were there more distinct commenters or likers? (Just for fun, not for credit: find the people who commented on at least one post but never liked any of them.)


# 7) Output a .csv file that lets you make scatterplots showing net positivity on x-axis and comment-counts and like-counts on y-axis. This will tell you whether your friends like your positive or negative posts more!

###################
import facebook
import json
import test

def pretty(obj):
    return json.dumps(obj, sort_keys=True, indent=2)

pos_ws = []
f = open('positive-words.txt', 'r')

for l in f.readlines()[35:]:
    pos_ws.append(unicode(l.strip()))
f.close()

neg_ws = []
f = open('negative-words.txt', 'r')
for l in f.readlines()[35:]:
    try:
        neg_ws.append(unicode(l.strip()))
    except:
        print l.strip()
    
class Post():
    """object representing status update"""
    def __init__(self, post_dict):
        if 'message' in post_dict:
            self.message = post_dict['message']
        else:
            self.message = ""
        # if the post dictionary has a 'comments' key, set self.comments to the
        # list of comment dictionaries extracted from the corresponding comments dictionary. Otherwise, set self.comments to an empty list []
        if 'comments' in post_dict:
            self.comments = post_dict['comments']['data']
        else:
            self.comments = []
     
        # if the post dictionary has a 'likes' key, set self.likes to
        # the list of likes dictionaries from the corresponding likes dictionary.  Otherwise, set self.likes to None
        if 'likes' in post_dict:
            self.likes = post_dict['likes']['data']
        else:
            self.likes = []
    
    def emo_score(self):
        return self.positive() - self.negative()
        
    def positive(self):
        count = 0
        for w in self.message.split():
            if w in pos_ws:
                count = count + 1
        return count
                
    
    def negative(self):
        count = 0
        for w in self.message.split():
            if w in neg_ws:
                count = count + 1
        return count


sample = open('samplepost.txt').read()
sample_post_dict = json.loads(sample)
p = Post(sample_post_dict)
# comment out the next lines once you get the tests to pass 
# print pretty(p.comments)
# print pretty(p.likes)
test.testType(p.comments, 'list')
test.testEqual(len(p.comments), 4)
test.testType(p.comments[0], 'dictionary')
test.testType(p.likes, 'list')
test.testEqual(len(p.likes), 4)
test.testType(p.likes[0], 'dictionary')

p.message = "adaptive acumen abuses acerbic aches for everyone"
test.testEqual(p.positive(), 2)
test.testEqual(p.negative(), 3)
test.testEqual(p.emo_score(), -1)        
    
# 2) Get a json-formatted version of your last 100 posts on Facebook.

access_token = "copy and paste from https://developers.facebook.com/tools/explorer"
access_token = ""
graph = facebook.GraphAPI(access_token)
feed = graph.get_object("me/feed?limit=100")
#feed = graph.get_object("me/feed")

# 3) For each of those posts
    # -- create an instance of your class Post, filling in attributes for:
        #-- the message if it exists
        #-- the comments data if it exists
        #-- the likes data if it exists

posts = []
for c in feed['data']:
    p = Post(c)
    posts.append(p)

# 4) Compute the top three likers and commenters on your posts (the people who did the most comments and likes)

commenters = {}
likers = {}
for p in posts:
    for comment in p.comments:
        nm = comment['from']['name']
        if nm in commenters:
            commenters[nm] = commenters[nm] + 1
        else:
            commenters[nm] = 1
    for like in p.likes:
        nm = like['name']
        if nm in likers:
            likers[nm] = likers[nm] + 1
        else:
            likers[nm] = 1

print "Top commenters-----"
for c in sorted(commenters.keys(), key = lambda x:commenters[x], reverse = True)[:3]:
    print "\t%s: %d" % (c, commenters[c])

print "Top likers------"
for l in sorted(likers.keys(), key = lambda x: likers[x], reverse=True)[:3]:
    print "\t%s: %d" % (l, likers[l])
  
# 5) Were there more distinct commenters or likers? (Just for fun, not for credit: find the people who commented on at least one post but never liked any of them.)
  
print "%d commenters; %d likers" % (len(commenters.keys()), len(likers.keys()))
# for fun: did anyone comment on a post but not like any of your posts?
count = 0
for p in commenters.keys():
    if p not in likers.keys():
        count = count + 1
print "%d people commented without ever liking" % count

# 7) Output a .csv file that lets you make scatterplots showing net positivity on x-axis and comment-counts and like-counts on y-axis. This will tell you whether your friends like your positive or negative posts more!

g = open('emo_scores.csv', 'w')
g.write('emo_score, comment_count, like_count\n')
for p in posts:
    try:
        g.write("%d, %d, %d\n" % (p.emo_score(), len(p.comments), len(p.likes)))
    except:
        print p.emo_score(), type(p.comments), type(p.likes)





