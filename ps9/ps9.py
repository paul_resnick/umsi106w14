###################
import facebook
import json
import test

def pretty(obj):
    return json.dumps(obj, sort_keys=True, indent=2)

pos_ws = []
f = open('positive-words.txt', 'r')

for l in f.readlines()[35:]:
    pos_ws.append(unicode(l.strip()))
f.close()

neg_ws = []
f = open('negative-words.txt', 'r')
for l in f.readlines()[35:]:
    neg_ws.append(unicode(l.strip()))

# 1) Fill in the definition of the class Post to hold information about one post that you've made on Facebook.
    # Add to the __init__ method additional code to set the instance variables comments and likes. 
    # You need to pull out the appropriate data from the json representation of a single post. 
    # You can find a sample in the file samplepost.txt. There are tests that check whether you've pulled out the right data.
    
class Post():
    """object representing status update"""
    def __init__(self, post_dict):
        if 'message' in post_dict:
            self.message = post_dict['message']
        else:
            self.message = ""
        # if the post dictionary has a 'comments' key, set self.comments to the
        # list of comment dictionaries extracted from the corresponding comments dictionary. 
        # Otherwise, set self.comments to an empty list []
        # Something similar has already been done for the contents of the original post, 
        # which is the value of the 'message' key in the dictionary, when it is present 
        # (photo posts don't have a message). 
        # See above for a model. But pulling out the list of dictionaries from a post_dict
        # is a little harder. Take a look at the sample of what a post_dict looks like
        # in the file samplepost.txt
     
        # Similarly, if the post dictionary has a 'likes' key, set self.likes to
        # the list of likes dictionaries from the corresponding likes dictionary.  
        # Otherwise, set self.likes to an empty list, []
    
    
    def emo_score(self):
        return 0
        
    def positive(self):
        return 0
                   
    def negative(self):
        return 0


sample = open('samplepost.txt').read()
sample_post_dict = json.loads(sample)
p = Post(sample_post_dict)
# use the next lines if you're having trouble getting the tests to pass.
# they will help you understand what a post_dict contains, and what
# your code has actually extracted from it and assigned to the comments 
# and likes instance variables.
#print pretty(sample_post_dict)
#print pretty(p.comments)
#print pretty(p.likes)
try:
    test.testType(p.comments, 'list')
    test.testEqual(len(p.comments), 4)
    test.testType(p.comments[0], 'dictionary')
    test.testType(p.likes, 'list')
    test.testEqual(len(p.likes), 4)
    test.testType(p.likes[0], 'dictionary')
except:
    print "One or more of the test invocations is causing an error,\nprobably because p.comments or p.likes has no elements..."

# 2) In the Post class, fill in three methods:
    # -- positive() returns the number of words in the message that are in the list of positive words pos_ws (provided in our code)
    # -- negative() returns the number of words in the message taht are in the list of negative words neg_ws (provided in our code)
    # -- emo_score returns the difference between positive and negative scores
    
    # make the three tests pass as a way of checking that you have defined the methods correctly

p.message = "adaptive acumen abuses acerbic aches for everyone"
test.testEqual(p.positive(), 2)
test.testEqual(p.negative(), 3)
test.testEqual(p.emo_score(), -1)        
    
# 3) Get a json-formatted version of your last 100 posts on Facebook. 
# (Hint: use the facebook module, as presented in class, and use https://developers.facebook.com/tools/explorer to get your access token. 
# Every couple hours you will need to get a new token.)

# 4) For each of those posts
    # -- create an instance of your class Post, filling in attributes for:
        #-- the message if it exists
        #-- the comments data if it exists
        #-- the likes data if it exists

# 5) Compute the top three likers and commenters on your posts 
# (the people who did the most comments and likes)
  
# 6) Were there more distinct commenters or likers? 
# (Just for fun, not for credit: find the people who commented on at least 
# one post but never liked any of them.)
  
# 7) Output a .csv file that lets you make scatterplots showing 
# net positivity on x-axis and comment-counts and like-counts on y-axis. 
# Can you see any trend in whether your friends are more likely 
# to respond to positive vs. negative posts? 
# Post a screenshot of your scatterplot to the facebook group.

# You can see what the .csv file's contents should look like in 
# the file emo_scores.csv. And you can see what the scatterplot 
# looks like in emo_scores.xlsx. In my case, there's not an obvious
# correlation between positivity and how many responses I got.






