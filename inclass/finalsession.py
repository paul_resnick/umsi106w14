##tuples

L = [1, 2, 3]
t = (1, 2, 5)
print type(t)
print t[0]
for x in t:
    print x*x

# t[0] = 7   #causes an error

x, y = 3, 8
print y # 8
(x, y) = (5, 6) # same idea as previous line, different syntax
print y #6

def f():
    return 9, 10
    
(x, y) = f()
print y # 10

def g():
    return "hello", "goodbye"
    
z = g()
print type(z)  #tuple
print z[0] #hello
print z[1] # goodbye

### string interpolation

s = "formatting string with %.1f %s to %s for" % (2.356,"things", "substitute")
print type(s) # a string
print s

print 9 % 4  # remainder (modulus) operation

####try/except

L = [1, 0, 3]
try:
    # some operation that might give an error
    for x in L:
        print 10.0 / x
    print "other stuff"
except:
    print "Oops, divided by zero"

print "At least the rest of the program can go on."

L = [2, 0, 5]
for x in L:
    try:
        # some operation that might give an error
        print 10.0 / x
    except:
        print "Oops, divided by zero"

print "Really moving on now."
    
####sorting
L = [23, 11, 42]
print sorted(L)
print sorted(L, key = lambda x: x % 10)  # [11, 42, 23]
## look at the textbook exercises on sorting instances of a class

## list comprehensions

print [2 for x in L]
print [x - 1 for x in L]
print [x + 1 for x in L]
print map(lambda x: x +2, L) # like the line above

print [2 for x in L if x < 20]  # prints [2]
print [x for x in L if x < 20]  # prints [11]
print filter(lambda x: x < 20, L) # equivalent to line above

print [x+1 for x in L if x < 20]  # prints [12]
print map(lambda x: x+1, filter(lambda x: x < 20, L)) # same thing as line above

### classes

class myClass:
    def __init__(self, y="mine"):
        print "hello"
        self.iv = y
        self.other_method()
    
    def other_method(self):
        print "yes"

x = myClass("hohum") # creates an instance; calls __init__ on that instance; returns the instance
print type(x)
x.other_method()  #prints yes
print x.iv # print hohum
q = myClass() # default value is "mine"
print q.iv
        
#### test cases #####
import test

# format of return value test
# test.testEqual(f(x), y)

# format of side effect test
# f(L) # suppose f appends to L or otherwise changes it 
# test.testEqual(L, z)
test.testType(3, "string")
x=0
test.testEqual(type(x), type(0))

def f(x):
    return x -1
    
test.testEqual(f(3), 2)




