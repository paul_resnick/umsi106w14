import tweepy
import webbrowser
import sys

##Paste your API_KEY and API_SECRET from the app page for the app you created on apps.twitter.com
API_KEY = ""
API_SECRET = ""

auth = tweepy.OAuthHandler(API_KEY, API_SECRET)
try:
    redirect_url = auth.get_authorization_url()
except tweepy.TweepError:
    print 'Error! Failed to get request token.'
webbrowser.open(redirect_url)
verifier = raw_input('Please input the verifier that you get after logging in in the browser window that just opened: ')
try:
    auth.get_access_token(verifier)
except tweepy.TweepError:
    print 'Error! Failed to get access token.'

api = tweepy.API(auth)

public_tweets = api.home_timeline()
for tweet in public_tweets:
    # I'm calling encode on tweet.text before printing because otherwise I get an error when there are non-latin characters in a tweet. This really only affects things when printing to some terminal windows; the international characters are fine for saving to files.
    print tweet.text.encode(sys.stdout.encoding, 'replace')