import urllib2
import webbrowser

show_print_statements = True

# Setup basic URLS 
base_url = "http://annarbor.craigslist.org/"


class Posting:
    def __init__(self, path=None):
        if path.find("http://") >= 0:
            self.url = path
        else:
            self.url = base_url + path
        self.description = ""
        self.loaded = self.get_posting()

    def get_posting(self):
        if show_print_statements:
            print "Loading "+ self.url
        try:
            result = urllib2.urlopen(self.url)
            contents = result.read()
        except Exception, e:
            if show_print_statements:
                print "tried to load " + self.url
            return False
        # For sample contents, see view-source:http://annarbor.craigslist.org/sof/4308813472.html
        idx = contents.find("postingbody")
        if idx >= 0:
            # drop text up to postingbody
            contents = contents[idx:]
            # get rid of everything before the closing bracket (>)
            idx = contents.find(">")
            contents = contents[idx+1:]
            # find the end of the section
            end = contents.find("</section>")
            # save that as the job description
            self.description = contents[:end]
        return True
    
    def python_mentioned(self):
        return self.description.find("python") >= 0
        
    def mentions(self, terms):
        for term in terms:
            if self.description.find(term) >= 0:
                return True
        return False #none of the terms are mentioned

######### OUR EXERCISE ##########
# Today we are going to 'scrape' craigslist for interesting data
# Specifically we are going to pull information about software jobs from the Ann Arbor craigslist

# Scraping is what coders did when they wanted information from the internet before APIs became common.
# Scraping is usually not elegant, but its pretty fun.

# To see what the data looks like, view-source:http://annarbor.craigslist.org/sof
# Another useful way to look at the webpage is to use the "webpage inspector" (most browsers have one)

# We want to open a URL for all of these pages
# -- then get the detailed information on those pages
# -- because that is where the interesting information is

# To find the link for each post, we would find each occurance of a link,
# and then parse out the href property (the link)
# Since each post has this form:
# <p class="row" data-pid="4403963177"> <a href="/sof/4403963177.html" class="i" data-id="0:00K0K_97uPCciZMuA"></a></p>
# Need to extract /sof/4403963177.html from it

category = "sof"
url = base_url+category # get the url for the software developer jobs
result = urllib2.urlopen(url)
contents = result.read()

jobs = []
pid_idx = contents.find("data-pid")
while pid_idx>=0:
    # get rid of all text up to next occurrence
    contents = contents[pid_idx:]
    # now we are going to find the closing bracket (>)
    idx = contents.find('>')
    # and then get rid of all the text before that
    contents = contents[idx+1:] # The +1 means we are looking at everything after
    
    # the next step is to see if this post has a link in it
    # some don't -- trust me
    # we do this by comparing the position of the href property against the closing </p> tag

    href_idx = contents.find("href")
    closing_idx = contents.find("</p>")
    if href_idx < closing_idx:
        # There is a link, so we want to pull the link out
        contents = contents[href_idx:]
        # Then lets find the first quotation mark (")
        idx = contents.find('"')
        contents = contents[idx+1:]
        # then find the second quotation mark (")
        end_idx = contents.find('"')
        # Everything inbetween should be our url
        post_url = contents[:end_idx]
    
        # and put that URL into our post class
        post = Posting(post_url)
        jobs.append(post)

    # now we try to move to the next post
    pid_idx = contents.find("data-pid")



# count how many jobs want programmers
py_jobs = [job for job in jobs if job.mentions(["python", "perl", "php"])]

print "%d of %d jobs want you to know python, perl, or php" % (len(py_jobs),len(jobs))

for job in py_jobs:
    webbrowser.open(job.url)

# count how many jobs want a bachelor's degree
py_jobs = [job for job in jobs if job.mentions(["bachelor's", "BA", "BS", "B.A.", "B.S."])]
print "%d of %d jobs want a bachelor's degree" % (len(py_jobs),len(jobs))

# count how many jobs want a master's degree
py_jobs = [job for job in jobs if job.mentions(["master's", "MS", "M.S.", "MA", "M.A."])]
print "%d of %d jobs want a master's degree" % (len(py_jobs),len(jobs))

# count how many jobs want a PhD
py_jobs = [job for job in jobs if job.mentions(["Ph.D.", "PhD"])]
print "%d of %d jobs want a Ph.D." % (len(py_jobs),len(jobs))
