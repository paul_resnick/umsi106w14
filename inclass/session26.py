import test

def square(x):
    return x*x
    
test.testEqual(square(3), 9)

def update_counts(letters, counts_dict):
    for c in letters:
        if c in counts_dict:
            counts_dict[c] = counts_dict[c] + 1
        else:
            counts_dict[c] = 1

counts_dict = {'a': 3, 'b': 2}
update_counts("aaab", counts_dict)
test.testEqual(counts_dict['a'], 6)

class Point:
    """ Point class for representing and manipulating x,y coordinates. """

    def __init__(self, initX, initY):

        self.x = initX
        self.y = initY

    def distanceFromOrigin(self):
        return ((self.x ** 2) + (self.y ** 2)) ** 0.5

    def move(self, dx, dy):
        self.x = self.x + dx
        self.y = self.y + dy

p = Point(3, 4)
test.testEqual(p.x, 3)
test.testEqual(p.y, 4)

test.testEqual(p.distanceFromOrigin(), 5.0)
p.move(-2, 3)
test.testEqual(p.x, 1)
test.testEqual(p.y, 7)

#### Excercises to try before class; you'll have some time to work on these during class as well. ####

def maxabs(L):
    """L should be a list of numbers (ints or floats). The return value should be the maximum absolute value of the numbers in L."""
    return max(L, key = abs)
    
#1. Create test cases for L. What would be a typical case? What would we extreme cases?

#2. We have usually used the sorted function, which takes a list as input and returns a new list containing the same items, possibly in a different order. There is also a method called sort for lists. It mutates the list, changing the order of the items, and returns the value None. Write some test cases for the sort method. What would be a typical case? What would be interesting extreme cases?

pos_ws = []
f = open('positive-words.txt', 'r')

for l in f.readlines()[35:]:
    pos_ws.append(unicode(l.strip()))
f.close()

neg_ws = []
f = open('negative-words.txt', 'r')
for l in f.readlines()[35:]:
    try:
        neg_ws.append(unicode(l.strip()))
    except:
        print l.strip()

class Post():
    """object representing status update"""
    def __init__(self, post_dict):
        if 'message' in post_dict:
            self.message = post_dict['message']
        else:
            self.message = ""
        # if the post dictionary has a 'comments' key, set self.comments to the
        # list of comment dictionaries extracted from the corresponding comments dictionary. Otherwise, set self.comments to an empty list []
        if 'comments' in post_dict:
            self.comments = post_dict['comments']['data']
        else:
            self.comments = []
     
        # if the post dictionary has a 'likes' key, set self.likes to
        # the list of likes dictionaries from the corresponding likes dictionary.  Otherwise, set self.likes to None
        if 'likes' in post_dict:
            self.likes = post_dict['likes']['data']
        else:
            self.likes = []
    
    def emo_score(self):
        return self.positive() - self.negative()
        
    def positive(self):
        return len([w for w in self.message.split() if w in pos_ws])
                
    
    def negative(self):
        return len([w for w in self.message.split() if w in neg_ws])

sample = open('samplepost.txt').read()
# 3. Write test cases for the Post class, taken from PS10.
# Use the sample data in the variable sample as the test input for creating an instance of Post.
