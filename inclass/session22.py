things = [3, 5, -4, 7]
# write some code that applies map to things, 
# passing different functions for transforming the
# items

def lengths(strings):
    """lengths takes a list of strings as input and returns a list of numbers that are the lengths
    of strings in the input list. Use map!"""
  # fill in this function's definition to make the test pass.

import test
test.testEqual(lengths(["Hello", "hi", "bye"]), [5, 2, 3])

# write some expressions using the filter function 

def longwords(strings):
    """Return a shorter list of strings containing only the strings with more than four characters. Use the filter function."""
    # write your code here
    
test.testEqual(longwords(["Hello", "hi", "bye", "wonderful"]), ["Hello", "wonderful"])

#### exercises on list comprehensions

# rewrite the functions lengths using a list comprehension

# rewrite the function longwords using a list comprehension

# combine them in a single function, using a single list comprehension, that returns the lengths of those strings that have at least 4 characters.

##### exercise using reduce

# write a function that takes a list of numbers and returns the sum of the squares of all the numbers. Use reduce.

# Now write an alternative version of that same function that uses map to get a list of the squared values and then just uses the built-in sum function, rather than using reduce.