# see https://docs.python.org/2.7/library/datetime.html
import time
import datetime

print "--- producing a datetime instance and extracting info from it ----"
then = datetime.datetime(2013, 8, 4, 12, 30, 45)
print then
print repr(then)
print type(then)
print then.year, then.month, then.day
print then.hour, then.minute, then.second
print then.microsecond

print
print "---- now() and utcnow()------"
now = datetime.datetime.now()
print now
print datetime.datetime.utcnow()

print
print "---- adding a week's timedelta to a datetime ----"   
oneweek = datetime.timedelta(days=7)
print now
print now + oneweek

print
print "---- using strftime to extract useful info from a datetime----"
print now.strftime("%Y%m%d T%H%M%S")
print "Or like this: " , now.strftime("%y-%m-%d-%H-%M")
    
print "Current year: ", now.strftime("%Y")
print "Month of year: ", now.strftime("%B")
print "Week number of the year: ", now.strftime("%W")
print "Weekday of the week: ", now.strftime("%w")
print "Day of year: ", now.strftime("%j")
print "Day of the month : ", now.strftime("%d")
print "Day of week: ", now.strftime("%A")

print
print "----Creating a datetime object from a string----"
str = '17/Dec/2011:09:48:49 -0600'
print str
print  datetime.datetime.strptime(str,"%d/%b/%Y:%H:%M:%S -0600")
