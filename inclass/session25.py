L = ["Cherry", "Apple", "Blueberry"]

print sorted(L)
print sorted(L, key=len)
#alternative form using lambda, if you find that easier to understand
print sorted(L, key= lambda x: len(x))   

class Fruit():
    def __init__(self, name, price):
        self.name = name
        self.price = price
        
    def sort_priority(self):
        return self.price
        
L = [Fruit("Cherry", 10), Fruit("Apple", 5), Fruit("Blueberry", 20)]
print "-----original sort order----"
for f in L:
    print f.name
print "------sorted by price------"   
for f in sorted(L, key = lambda x: x.price):
    print f.name

print "-----sorted by price, referencing a class method-----"
for f in sorted(L, key = Fruit.sort_priority):
    print f.name
    
print "---- one more way to do the same thing-----"
for f in sorted(L, key = lambda x: x.sort_priority()):
    print f.name
    
###In-class exercise ####

class Point:
    """ Point class for representing and manipulating x,y coordinates. """

    def __init__(self, initX, initY):

        self.x = initX
        self.y = initY

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def distanceFromOrigin(self):
        return ((self.x ** 2) + (self.y ** 2)) ** 0.5
    
    def move(self, dx, dy):
        self.x = self.x + dx
        self.y = self.y + dy
        
    def __repr__(self):
        return "(%d, %d)" % (self.x , self.y)

points = [Point(3,1), Point(3, 5), Point(-2,1), Point(4, -2)]
print points
# sort the points by their x coordinates
# sort the points by increasing distance from the origin