# TO RUN THIS
# for mac:
# sudo pip install nltk
# for windows:
# /c/Python27/Scripts/pip install nltk

import nltk
from nltk.corpus import names # this is a corpus of names in NLTK
import random

# see http://www.nltk.org/book/ch05.html, Table 5.1 for a list of part of speech (POS) tags and what their abbreviations are in this system! e.g. 'ADJ' stands for "adjective", 'VN' stands for "past participle" ... all based on linguistics conventions
# (if you want to talk about this more, I'd love to and can point you in the direction of more resources!)

# Tokenizing and Part of Speech tagging

f = open("fbdata2.txt", 'r')
msgs = [x[6:].strip() for x in f.readlines() if x[:5] == "post:"]
#print msgs[:3]

## UNCOMMENT the following lines in order to look at an example of tokenizing message texts and splitting them into part-of-speech tagged lists
# for m in msgs[:5]:
# 	msg_words = nltk.word_tokenize(m) # word_tokenize is like a smarter split method that handles punctuation
# 	# (you could write a version of this!)
# 	print nltk.pos_tag(msg_words) # this prints a list of tuples of (word, POS) form

# so if you wanted to count all the nouns in a facebook message from fbdata2.txt
one_msg = nltk.word_tokenize(msgs[0])

def count_nouns(msg):
	noun_names = ['NNP','NN']
	return len([x for x in nltk.pos_tag(msg) if x[1] in noun_names])

print "How many nouns?"
print count_nouns(one_msg)

# What message in fbdata2.txt has the most nouns? 
# What about the most verbs? 
