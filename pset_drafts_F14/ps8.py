## Problem Set 8
import test


## Please write your name here in a comment -- makes it easier for us grading!

##########

# Below is some code we've provided for you, some of which you'll have to add a little to. 
# As always, look through it and understand it, make comments, test it, pull it apart in your head
# (or in a blank .py file!).

# This code gets all the text from A Study in Scarlet by Sir Arthur Conan Doyle and saves
# it in the variable scarlet_text.
f = open("study_scarlet.txt","r")
scarlet_text = f.read()

# This code separates that text into training and testing data. 
# (Remember from the textbook what that means!)
# Training data is stored in the variable train. Test data is stored in the variable test.
lp = len(scarlet_text)
len_third = lp/3 
train = lp[len_third]
test = lp[len_third:]

# Here is the guesser function you saw in the textbook. 
# You'll be using this, with some rules you create, to play the Shannon Game.
def guesser(prev_txt, rls):
   all_guesses = ""
   for (f, guesses) in rls:
      if f(prev_txt):
         all_guesses = all_guesses + guesses
   return all_guesses

## List of rules -- you'll be adding to this list!
rules = [(lambda x: x[-1] == "q", "uai"),
         (lambda x: True, "abcdefghijklmnopqrstuvwxyz")]

## Here are some sample calls to the guesser function, like the ones provided in the textbook,
## if you want to play with it and remember what it does.
# print guesser(" ", rules)
# print guesser(" The q", rules)
# print guesser(" The qualit", rules)


# --------------

# [PROBLEM 1]

# Write a new rule (you can define a function, or write a lambda expression like the rules above) 
# that will return capitals first if the previous text is a space.
# The rule should be a tuple, but the first argument can be either a function
# that you define here, or it can be a lambda expression.
# HINT: Model this after the rule that determines what comes after q!
# Save that tuple rule into the variable caps_rule.

# Put your code here!


# This code will add your rule to the list of rules:
# (Make sure you've used all the right variable names, like caps_rule, or else this won't work.)
try:
	rules.insert(1,caps_rule)
except:
	print "The caps_rule tuple does not exist yet."

# Tests for caps_rule
print "Testing for caps_rule tuple contents"
try:
	test.testEqual(caps_rule[1],"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
except:
	print "the caps_rule tuple does not exist yet or has the wrong value"

# --------------

# [PROBLEM 2]

# Remember the letter_frequencies function you wrote for Hangman? You're going to use that,
# or write it again, and use it in a rule that will guess letters in order of their
# frequency in the TRAINING SET of A Study in Scarlet. (Stored in the variable train.)

# Write a rule that will guess letters in order of their frequency in the train data,
# no matter what the previous text is.
# HINT: You will probably want to sort the letters in a list and use the .join() method to 
# get a string of guesses for the second element of your tuple.
# (By 'string of guesses', we mean like the string "abcdefghijklmnopqrstuvwxyz" in the 
## last rule in your rules list.)

# Save your new tuple rule in the variable freqs_rule.

# Put your code here!


# This code will add your rule to the list of rules:
try:
	rules.insert(-2,freqs_rule)
except:
	print "The freqs_rule tuple does not exist yet."


# TODO fill in test
print "testing for freqs_rule"
try:
	print "test"
except:
	pass


# [PROBLEM 3]

# Here is code to create a dictionary to store the frequency of letters that follow 
# each letter in the alphabet, using the train study in scarlet text.
# Go through this and understand it -- do not change it!

alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZ -,.!?_:()" #
freq_ch_track = {}
for ind in range(len(train)):
	ch = train[ind]
	print ch
	if ch in alph and ch in freq_ch_track:
		if len(train) == ind+1:
			break # don't worry about this -- this means we break out of the loop and are done
			# this is in the case where we've gotten to the end of the text
		elif train[ind+1] in freq_ch_track[ch]:
			print train[ind+1]
			freq_ch_track[ch][train[ind+1]] += 1
		else:
			freq_ch_track[ch][train[ind+1]] = 1
	elif ch in alph and ch not in freq_ch_track:
		freq_ch_track[ch] = {}
	# but if it's a character not in the alphabet we care about, don't track it
	# hence no else clause


# [PROBLEM 4]

# The structure of the dictionary we created there looks like this:
# {'a':{'b':2,'c':4}, 'd':{'e':6,'a':7}}
# if, for example, b came after a 2 times in the data, and c came after a 4 times
# in the data, e came after d 6 times, and so on.
# You might want to try printing out several different values or
# iterating through the dictionary's keys in different ways to make sure
# you understand the structure.

# Below is code that creates a rule to guess the most frequent 
# next-letter for each letter, based on the dictionary we created
# from the training data.

# Write a comment to explain what each line is doing.





