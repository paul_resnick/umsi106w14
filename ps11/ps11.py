#####Part I. Describe your project #####

# Include:
# a.	What data sources you will use
# b.	How you plan to process the data you will get.
# c.	How you will present the results.
s = """Project description goes here"""
print s
print
print "------"

#####Part II: Describe a class you willl define ####

# The name of my class will be...
your_name = ""

# Each instance of my class will represent one...
your_inst_represents = ""

# Each instance of my class will have ... instance variables
your_inst_var_count = 0

# Each instance will have instance variables that keep track of...
your_inst_vars = ""

# One method of my class, other than __init__, will be named...
your_method_name = ""

# When invoked, that method will...
your_method_description = ""

print "The name of my class will be %s. Each instance of my class will represent one %s. Each instance will have %d instance variables. The instance variables will keep track of %s. One method of my class, other than __init__, will be named %s. When invoked, that method will %s." % (your_name, your_inst_represents, your_inst_var_count, your_inst_vars, your_method_name, your_method_description)
print
print "----------"

#####Part III: Code to retrieve data and extract something from it #####

#See requirements in the "Final Project Instructions" document in cTools

